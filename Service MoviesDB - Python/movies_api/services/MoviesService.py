from repositories.MoviesRepository import MoviesRepository

class MoviesService(object):
    def __init__(self):
        self.Movies_repository = MoviesRepository()

    def add_Movie(self,name,description,stars,year):
        return self.Movies_repository.add_Movie(name,description,stars,year)

    def get_all_Movies(self, page, pagesize, name, description, stars, year):
        print("...service....name: " +name)
        return self.Movies_repository.get_all_Movies(page, pagesize, name, description, stars, year)

    def get_Movie_by_id(self, id):
        return self.Movies_repository.get_Movie_by_id(id)

    def update_Movie(self, id, name, description, stars, year):
        return self.Movies_repository.update_Movie(id, name, description, stars,year)

    def delete_Movie(self, id):
        return self.Movies_repository.delete_Movie(id)
