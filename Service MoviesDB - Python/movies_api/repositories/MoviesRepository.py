import pymysql
from db_config import mysql

class MoviesRepository(object):
    def __init__(self):
        self.conn = mysql.connect()

    def add_Movie(self,name,description,stars,year):
        sql = "INSERT INTO movies(name,description,stars,year) VALUES(%s,%s,%s,%s)"
        data = (name,description,stars,year,)
        cursor = self.conn.cursor()
        cursor.execute(sql, data)
        self.conn.commit()
        lastrowid = cursor.lastrowid
        cursor.close()
        return lastrowid

    def get_all_Movies(self, page, pagesize, name, description, stars, year):
        print(pagesize, name, description, stars, year)
        startat = page*pagesize
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        if name:
            print("ENTRE EN NAME")
            cursor.execute("SELECT id, name, description, stars, year FROM movies WHERE name LIKE %s LIMIT %s, %s", (name,startat, pagesize))
        else:
            print("ENTRE EN ELSE")
            cursor.execute("SELECT id, name, description, stars, year FROM movies LIMIT %s, %s", (startat, pagesize))

        rows = cursor.fetchall()
        cursor.close()
        return rows

    def get_Movie_by_id(self, id):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT id, name, description, stars, year FROM movies WHERE id=%s", id)
        row = cursor.fetchone()
        cursor.close()
        return row

    def update_Movie(self, id, name, description, stars, year):
        sql = "UPDATE movies SET name=%s, description=%s, stars=%s, year=%s WHERE id=%s"
        data = (name, description, stars, year,id,)
        cursor = self.conn.cursor()
        rows_affected = cursor.execute(sql, data)
        self.conn.commit()
        cursor.close()
        return rows_affected

    def delete_Movie(self, id):
        cursor = self.conn.cursor()
        rows_affected = cursor.execute("DELETE FROM movies WHERE id=%s", (id,))
        self.conn.commit()
        cursor.close()
        return rows_affected