from flask import Blueprint
from app import app
from services.MoviesService import MoviesService
from flask import jsonify
from flask import flash, request

movies_api = Blueprint('movies_api', __name__)
movies_service = MoviesService()

@movies_api.route('/movie', methods=['POST'])
def add_Movie():
    try:
        _json = request.json
        _name = _json['name']
        _description = _json['description']
        _stars = _json['stars']
        _year = _json['year']
        print(_json,_name,_description,_stars,_year)
        # validate the received values
        if _year and _description and _name and _stars and request.method == 'POST':
            lastrowid = movies_service.add_Movie(_name,_description,_stars,_year)
            print (lastrowid)
            resp = jsonify({'id': lastrowid})
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
        print(_json,_name,_description,_stars,_year)

@movies_api.route('/movie', methods=['GET'])
def get_all_Movies():
    try:
        page = request.args.get('page', default = 1, type = int)
        name = request.args.get('name', default = None, type = str)
        description = request.args.get('description', default = None, type = str)
        stars = request.args.get('stars', default = None, type = int)
        year = request.args.get('year', default = None, type = int)
        app.logger.info("page: " + str(page))       
        pagesize = 2
        rows = movies_service.get_all_Movies(page, pagesize, name, description, stars, year)
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@movies_api.route('/movie/<int:id>', methods=['GET'])
def get_movie_by_id(id):
    try:
        row = movies_service.get_Movie_by_id(id)
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@movies_api.route('/movie/<int:id>', methods=['PUT'])
def update_Movie(id):
    try:
        _json = request.json
        _name = _json['name']	
        _description = _json['description']	
        _stars = _json['stars']	
        _year = _json['year'] 	
        # validate the received values
        if _name and id and _description and _stars and _year and request.method == 'PUT':
            rows_affected = movies_service.update_Movie(id, _name, _description, _stars, _year)
            app.logger.info("PUT update_Movie, rows_affected: " + str(rows_affected))
            if rows_affected == 0:
                resp = jsonify({'message': 'Movie was not updated!'})
                resp.status_code = 200
            else:
                resp = jsonify({'message': 'Movie updated successfully!'})
                resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)

@movies_api.route('/movie/<int:id>', methods=['DELETE'])
def delete_movie(id):
    try:
        rows_affected = movies_service.delete_rol(id)
        if rows_affected == 0:
            resp = jsonify({'message': 'Movie was not deleted!'})
            resp.status_code = 200
        else:
            resp = jsonify({'message': 'Movie deleted successfully!'})
            resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@movies_api.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found',
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp